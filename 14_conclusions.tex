%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           Chapter V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{\uppercase{Conclusions}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Applicability of AMR}

The results presented in the previous sections show that using Multimesh AMR, on various problems that are of interest in nuclear engineering, can be advantageous. The advantages appear in the size of numerical system that is required to be solved, and the time required to reach a solution. In every case tested, the system constructed using Multimesh AMR was about $10\times$ smaller than the system constructed using a globally refined static mesh. In addition, the time required to assemble the system constructed by Multimesh AMR does not, in general, take longer than the savings from solving a smaller system.

The techniques explored through this Thesis are general enough that they could be adapted to other applications in nuclear engineering. One area where the propagating mesh could make an impact would be in the study of shock propagation through material. This application quantifies how discontinuities in material states move through a domain, similar to the behavior of the Traveling Gaussian solution. While the Traveling Gaussian solution does not posess the same properties as a shock front (i.e., the Gaussian solution is infinitely differentiable, while a shock front is not), the need for spatial refinement to move through the domain as the solution moves through the domain is similar in both solutions. The propagating mesh algorithm could potentially refine the spatial mesh around the shock front as it moved through the domain.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dynamic AMR}

The philosophy driving Adaptive Mesh Refinement is to produce a spatial mesh that is optimally suited for the specific solution. This philosophy can be extended to the temporal dimension in that the spatial mesh should change in time with the solution. Many of the articles presented in the introduction use a strategy of spatially refining at every time step. Additionally the refinement stopping criteria is based on being able to produce an accurate error estimator, which might not be available; instead, the strategy presented in this Thesis relies on the temporal behavior of an error indicator which is easily obtained. This strategy allows for a lightweight version of the strategies presented in the literature for the dynamic refinement of spatial meshes.

A concern for the propagating mesh strategy was that it would be dependent on the initial condition of the solution. Since the behavior of nuclear reactors during accidents (an important facet of nuclear engineering) has solutions which are initially smooth and develop a large localized discontinuity, this algorithm needs to be able to handle this situation. The Reactor manufactured solution was devised to address this concern directly. 

Based on the results presented in the previous chapter, this method is not dependent on the initial condition. Moreover, there is no reason to believe that there would be a dependence on the initial condition. Since the method measures how the error changes spatially, if a new feature to the solution is added there should be a large change in the error at that location. This method seems to be an appropriate method for solving the types of problems found in the nuclear engineering field.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Lessons Learned}

With the completion of any project, it is imperative to reflect on what revelations were conceived during the project; this step allows for the revelations to be applied to future projects. During the course of this Thesis, many important lessons were learned and two of the most influential will be discussed presently.

At the Graduate level, the concept of non-stationary linear solvers are introduced. While these solvers work very well for large matrix systems, they are generally not as efficient for small matrix systems as direct methods. An enticing trap to fall into as a young Graduate student is to use the non-stationary solvers in all cases. The first lesson learned during this Thesis was that when solving small matrix systems ($n \lesssim 10,000$), a direct solver will outperform an iterative solver. A large amount of time was spent at the beginning of this project waiting on the linear solver to converge until a direct solver was finally implemented.

The concept that the error from both spatial discretization and temporal discretization contribute to the overall solution error seems obvious. However, when performing convergence analysis on the solution error, this phenomena became painfully obvious. It was first discovered when testing the temporal convergence of the traveling Gaussian solution with a low solution speed; since the solution speed was low, the temporal error could be resolved quite well. The temporal convergence rate began to plateau when the time step size was made smaller. It became obvious to the author that if the temporal error could be resolved well, then the spatial error would contribute more to the total error; the hypothesis was tested with a positive outcome, by making the spatial mesh smaller. The second lesson learned during this Thesis was that the spatial mesh size and the temporal step size need to balance each other. One cannot simply reduce a portion of the error and expect that the overall solution error will behave as expected.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Future Work}

As with any project, there can always be improvements. This section outlines a few areas that could be expanded upon in the future.

This Thesis focused on spatial adaptivity, but another equally important concept is temporal adaptivity. If a solution is changing rapidly in time, a smaller time step size would be desired to better resolve the solution's behavior. However, if the solution is slowly varying a larger time step size can be taken to avoid unnecessary computations. Temporal adaptivity can be used to more efficiently utilize computational resources, just as in spatial adaptivity. A class of time integrator methods that are able to estimate the error introduced by the temporal discretization are Embedded Runge-Kutta (ERK) methods.  The implementation of ERK methods could also be used to obtain a more appropriate $\theta_\mathrm{tol}$. The error obtained at each time step remaining low (indicating that the solution is slowly varying in time) would trigger the use of a larger $\theta_\mathrm{tol}$, while the error remaining large would trigger the use of a smaller $\theta_\mathrm{tol}$. There might still be heuristics involved in determining the relationship between the ERK error and the size of $\theta_\mathrm{tol}$, but the heuristics would be more sophisticated.

In the presented dynamic mesh strategy, the spatial mesh size was limited by the number of initial refinements input by the user. This input is generally based on a heuristic estimate of how the solution will behave. In reality the spatial mesh size is only limited by the size of a time step. Moreover, the total solution error behaves as $\mathcal{O}(h^{p+1}+\tau^s)$ and thus to keep both parameters balanced the relation $h \sim \tau^\frac{s}{p+1}$ should hold. This could serve as an excellent limit on the spatial mesh size instead of a heuristic input. Additionally, if temporal adaptivity were implemented the mesh size limit would adjust to the time step size without user input.

In the quest for more accurate simulation techniques, the size of the system is likely to increase. In many cases, the memory consumption for the Jacobian matrix used in Newton's Method will become prohibitive. In such cases it is beneficial to use low-storage methods such as the Jacobian-Free Newton Krylov (JFNK) method. These methods use the fact that non-stationary linear solvers do not actually require access to the system matrix, but only require the action of the matrix on a given vector. Thus, a numerical approximation of the Jacobian can be made using the perturbation from vectors that span a Krylov subspace; using JFNK then only requires the residual to be stored.
 
An extension to the Propagating mesh algorithm could be to restart the solution process after spatial discretization. This extension would, in essence, send trial solutions to test how the solution will change; restarting the solution after spatial adaptation allows the AMR process to gather information on what the spatial mesh will need to be. Preliminary tests on this extension were conducted and produced solutions closer to what would be computed on a uniformly refined mesh. For the problem tested the gain in accuracy was not drastic, but for other types of problems the results could be different.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           SECTION I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\pagestyle{plain} % No headers, just page numbers
\pagenumbering{arabic} % Arabic numerals
\setcounter{page}{1}


\chapter{\uppercase {Introduction}}

Many of the physical processes occurring in the analysis of nuclear systems are strongly coupled. For example, fuel temperature affects the rate of fission in the fuel and the fission rate affects the temperature field --- mathematically this manifests through the temperature dependence of the macroscopic cross sections. When modeling nuclear systems these interactions present themselves as nonlinear coupling terms in the governing equations, which can be challenging to solve numerically. 

Additionally, solving for the analytical solution to these equations is not a realistic goal. The alternative is to solve a discrete approximation to the continuous problem; however, this introduces numerical error. The amount of error introduced can be reduced by decreasing the size of the spatial mesh used to approximate the solution. An effective way to generate a mesh that is optimized to the specific solution being sought is to use Adaptive Mesh Refinement (AMR). In this process, the spatial mesh is locally refined based on the solution's properties at that location and left unrefined in areas where refinement is not needed.

When modeling multiphysics phenomena, it is likely that the multiple components of the solution will have very different smoothness. It would be unreasonable to expect that a single mesh produced through AMR would be optimal for all solution components. A technology which has shown potential in multiphysics modeling is Multimesh AMR, where each solution component is allowed to have an independent mesh that is adapted to its features. This allows for each solution component to be computed on a spatial mesh that is optimal for that component.

A consequence of using AMR on time dependent problems is that the spatial mesh is required to change with the solution in time. Current algorithms address dynamic mesh refinement by spatially adapting the mesh at every time step; additionally, these algorithms may start the refinement process on a coarse mesh at every time step. This strategy can be useful when the solution is changing rapidly in time, but may be inefficient if the solution varies slowly in time. A method for using a spatial mesh for multiple time steps is developed and discussed in this Thesis.

The topics covered in this Thesis can improve how effectively computational resources are used while seeking the solution to a time-dependent problem. The savings in resources can be used to either solve a problem more accurately, or reach a solution quicker. In short, this Master's Thesis is a study of how to efficiently and accurately solve nonlinear, coupled, unsteady equations using Multimesh AMR technology.


\section{Motivation of Adaptively Refined Meshes}

The theory of numerical solutions to partial differential equations states that as the size of the mesh decreases, the error between the exact solution and the numerical solution also decreases at prescribed rates \cite{Hoffman-Numerical}. This means that in order to obtain a more accurate solution, the number of spatial cells, and thus the number of unknowns, should increase.

Consequently, when the number of unknowns increases, the wall-clock time to obtain a solution also increases. Solving large numerical systems is limited by how much computing power is present, the more computing power the larger the system size can be solved. What is desirable is a way to achieve the same level of numerical error with a smaller system. This can be done by choosing where to locally refine a spatial mesh to obtain the maximum reduction in error. This is the motivation behind Adaptive Mesh Refinement --- to achieve an accurate solution with fewer unknowns by locally refining a mesh rather than using a uniformly refined mesh.

If a solution has strong local features, it is likely that the solution error is concentrated in these locations of the domain; these local error contributions can dominate the solution error. Thus, if the cells in these locations are refined, then the dominant source of solution error can be reduced. If the mesh is not adapted in regions where the local error contribution is small, the system size, and thus the number of unknowns, will remain small. Furthermore, since the dominant source of error is being reduced without refining the mesh uniformly, the same level of error can be achieved with fewer unknowns.

Furthermore, when modeling multiple physical processes simultaneously, as is the case for many modern algorithms \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-Multimesh,Solin-DynamicMesh}, each physical process can be computed on an independent mesh. The illustration given in \cite{Solin-Multimesh} describes the different properties of a temperature field and a displacement field. The temperature field in a material is generally smooth, contrary to the displacement field which has singularities at re-entrant corners. Singular points require high levels of refinement to properly resolve the solution gradients; if the temperature field is required to use the same mesh as the displacement field, there will be far too much refinement for the temperature than is needed in these areas. Using independent meshes allows each solution to have a mesh that is optimized to its properties without influence from other solutions.

\section{The State-of-the-art in Nuclear Engineering}

The nuclear engineering field encompasses the study of many physical processes (particle transport, radiative heat transfer, thermo-mechanical stresses, etc.), each with some type of multiphysics phenomenon influencing the solution process. The accurate solution to these types of problems allow scientists and engineers to design and build better power generation stations, understand the inter workings of supernovae, and more \cite{AMP-Multiphysics,AstroPhysics-Neutrino,AstroPhysics-Multiphysics}. In all of these cases, the numerical systems can be extremely large. The advantage of using AMR technology is that it can achieve the same level of solution error with a smaller problem to solve and thus it can be a worthwhile technology to study and incorporate in the nuclear engineering field. 

Adaptive Mesh Refinement has slowly emerged in the nuclear engineering field in the last few years \cite{hp-Coupled, h-Yaqi, hp-Yaqi}. This technology has been used to solve for the criticality state of reactors modeled in two and three dimensions \cite{h-Yaqi, hp-Yaqi}. In these papers, the neutron diffusion equations with two energy groups were solved using Multimesh AMR; each energy group had an independent mesh with coupling between the two groups through neutron scattering and fission. Multimesh AMR is helpful in resolving the solution shape accurately, which is necessary to determine the criticality state of a nuclear system. 

AMR has also been used to solve coupled physics problems, in particular the coupling between the neutron flux and the temperature field \cite{hp-Coupled}. That study solves the time dependent, coupled, heat conduction and neutron diffusion equations and builds on the techniques developed in \cite{Dubcova-MicrowaveHeating,Solin-DynamicMesh}. The method used required that the spatial mesh be adapted at every time step. This can yield accurate results for problems changing drastically through the time evolution, but might be inefficient for problems that are not changing drastically at every time step.

\section{Improvements to the State-of-the-art}

The studies \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-DynamicMesh} can be considered what is the present state-of-the-art in multiphysics simulations applied to nuclear engineering; this Thesis extends directly from the methods described in these papers. Improvements made to the current state-of-the-art will address the use of spatial meshes for multiple time steps and the use of higher order time integrators. 

In the studies \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-DynamicMesh}, the spatial mesh was refined at every time step. This constraint can be useful for cases when a solution is changing rapidly, but can be excessive if the solution is slowly varying in time. An improvement can be made by allowing a given spatial mesh to be used for several time steps, and to only be adapted when the solution has changed by a significant amount. An algorithm is explored that quantifies how much a solution changes in time and spatially adapts when the solution has changed significantly.

The authors of \cite{hp-Coupled} remarked that using first order time discretization methods required the time step size to be small. The implementation of higher order time discretization methods allows for larger time steps to be taken while still maintaining a low solution error. A second order method was used in \cite{Solin-DynamicMesh}, but the implementation was fixed for that specific method. A general way to implement Runge-Kutta methods is explored in this Thesis.

The following outlines the structure for the remainder of the Thesis. Chapter 2 introduces numerical methods that will be useful to understand how the numerical systems discussed are built and solved. Special emphasis will be given to the complexities that arise when coupling terms are present between physical models and it is desired to solve each variable on an independent mesh. The end of Chapter 2 discusses the proposed algorithm for utilizing a spatial mesh for multiple time steps. Once the numerical tools have been introduced, the process of code verification is introduced in Chapter 3. Several exact solutions are considered to test various properties of the proposed algorithm. The Thesis ends with conclusions about the performance of the proposed algorithm and outlines possible extensions to this project.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Chapter II --- Advanced Mathematics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{\uppercase {Solution Techniques}}

The nuclear engineering field encompasses the understanding of many physical processes, which require many techniques to accurately solve equations that model these processes. A specific facet of the nuclear engineering field, namely reactor dynamics, will be explored to develop solution techniques that can serve a much broader purpose. This chapter details the methods used in this Thesis to accurately solve this model problem relevant to nuclear engineering, which will be a coupled, nonlinear, multiphysics model of neutron diffusion and heat conduction. The foundation of the solution techniques described in this chapter build on the formation of nonlinear residuals and Newton's Method, thus the first section of this chapter will introduce these concepts. The model problem is a set of time dependent partial differential equations which will require spatial and temporal discretization, thus the second and third sections will address discretization. The remainder of the chapter is dedicated to improvements made on the state-of-the-art in nuclear engineering.

Now is a good point to introduce the model problem studied during this Thesis. Equation \ref{eq:reactorModel} represents the one group neutron diffusion equation and the heat conduction equation. This model is simplified from reality, however it can be used to develop the solution techniques necessary for many types of multiphysics problems.

\begin{equation}
\begin{bmatrix}
\frac{1}{v} & 0 \\
0 & \rho C_p \\
\end{bmatrix}
\frac{\partial}{\partial t}
\begin{bmatrix}
\phi \\
T \\
\end{bmatrix} = 
\begin{bmatrix}
\nabla \! \cdot \! D \nabla - \Sigma_{a}(T) + \nu(1-\beta)\Sigma_{f} & 0 \\
\kappa & \nabla \! \cdot \! k \nabla \\
 \end{bmatrix}
\begin{bmatrix}
\phi \\
T \\
 \end{bmatrix}
+
\begin{bmatrix}
Q_{\phi} \\
Q_{T} \\
 \end{bmatrix}
\label{eq:reactorModel}
\end{equation}

The coefficient matrix in front of the time derivative will later be referred to as $G$; the coefficients $\frac{1}{v}$ and $\rho C_p$ are the inverse of the neutron velocity and the material heat capacity. The diffusion term in the neutronics equation, $\nabla D \nabla$, and the same term in the heat equation, $\nabla k \nabla$, are not dependent on the temperature as is in the most general case. The term $\Sigma_a(T)$ is the absorption coefficient that has a nonlinear dependence on temperature; the exact form of the absorption cross section's dependence on temperature is not important at this point. The term $\nu(1-\beta)\Sigma_f$ is the fission cross section. Notice that the coupling term $\kappa$ provides a linear dependence of temperature on the neutron flux. A more compact way of writing Equation \ref{eq:reactorModel} is given by denoting the solution vector $\vec{U} = [\phi \quad T]^T$ and making the right hand side be the steady state residual.

\begin{equation}
\frac{\partial \vec{U}}{\partial t}  = G^{-1} \tilde{f}(\vec{U},t)
\label{eq:reactorModelvec}
\end{equation}

Here $G$ is the diagonal matrix of coefficients that appear in front of the time derivatives in Equation \ref{eq:reactorModel}; for the rest of the discussion, the coefficient matrix $G$ will be incorporated in the residual $f = G^{-1}\tilde{f}$. Equations \ref{eq:reactorModel} \& \ref{eq:reactorModelvec} contains many challenging aspects --- tight couplings, nonlinear coefficients, etc. A variety of mathematical techniques will be needed to solve such a system of equations, which will be introduced in the remainder of this chapter. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section I --- Non-Linear Problems
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Newton's Method}
As stated in the introduction of this chapter, many of the physical processes that are of interest to nuclear engineers are nonlinear. This means that any methodology for solving equations related to nuclear engineering must include a way to resolve such nonlinear dependencies. Newton's method is an extensively studied method for solving nonlinear equations \cite{Iterative-Kelley,Newton-Kelley,Newton-Knoll,Newton-Lowrie} and is the choice of nonlinear solver for this Thesis.

Newton's method involves finding the roots of the nonlinear residual function [$F(\vec{U}) = 0$] formed after spatial and temporal discretization. Newton's method locates the roots of $F(\vec{U})$ by calculating the tangent plane of the residual function at the current iterate and locating the roots of this linear approximation of the residual function \cite{Newton-Kelley}. The sequence of a Newton iteration is given by

\begin{eqnarray}
J(\vec{U}^l) \delta \vec{U} = -F(\vec{U}^l), \nonumber \\
\vec{U}^{l+1} = \vec{U}^l + \delta \vec{U}, \nonumber
\end{eqnarray}

\noindent
where the Jacobian matrix $J(\vec{U}^l)$ is formed by differentiating the residual with respect to every unknown. The iteration process is terminated when the nonlinear residual is sufficiently small when compared to the initial residual.

\subsection{Formulation of Residual and Jacobian}
From Equation \ref{eq:reactorModel}, the nonlinear residual can be formed by moving all terms to one side of the equality sign. Thus the nonlinear residual would have the form

\begin{equation}
F(\vec{U}) = 
\begin{bmatrix}
F^{\phi} \\
F^{T} \\
\end{bmatrix},
\end{equation}

\noindent
where the residual is comprised of the residual from each equation in \ref{eq:reactorModel}; the order in which individual residuals are placed in the system is arbitrary.

The Jacobian matrix can be computed analytically or approximated numerically. For the Jacobian to be computed analytically, one needs access to the functional form of the residual and also needs to store the entries of the Jacobian matrix. In many applications either access to the governing equations or the storage size of the Jacobian is prohibitive; in such cases it is necessary to approximate the Jacobian. Using a direct linear solver requires the Jacobian to be approximated and stored, while an iterative Krylov solver allows the Jacobian to be approximated without storage \cite{Newton-Kelley}. In this Thesis, the Jacobian matrix is computed analytically because the governing equations and the functional form of the parameter and closure relationships are well known and can be easily differentiated; this is also an attempt to reduce the possibility of numerical errors. The storage size of the Jacobian may become prohibitive if more accurate solutions are desired; the use of low-storage methods (JFNK) can be considered for the future work of this project. To form the Jacobian matrix, each equation is differentiated with respect to each unknown variable. The Jacobian matrix takes the form

\begin{equation}
J(\vec{U}) = 
\begin{bmatrix}
\frac{\partial F^{\phi}}{\partial \phi} & \frac{\partial F^{\phi}}{\partial T} \\
\frac{\partial F^{T}}{\partial \phi} & \frac{\partial F^{T}}{\partial T}\\
 \end{bmatrix}
=
\begin{bmatrix}
J_{\phi \phi} & J_{\phi T} \\
J_{T \phi} & J_{T T} \\
 \end{bmatrix},
\end{equation}

\noindent
where the second notation is introduced for simplicity. The Jacobian in this form is a block sparse matrix and will require linear solvers. Since the problem sizes studied in this Thesis are relatively small, direct linear solvers will be used.

\subsection{Linear Solves}

Once the nonlinear system is formed, the linear system must be solved at every Newton iteration. Since the decision was made to analytically compute the Jacobian matrix, there is no restriction on the type of linear solver used. Early in this study, both GMRes and a direct linear solver were implemented. However, it was discovered that the linear system was small enough and this meant that a direct method was more efficient than a non-stationary iterative method. If the problem size grows, it is likely that the non-stationary iterative method will become more efficient either because the size of the system is large or that a Jacobian-free method will be implemented because of memory usage constraints.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section II --- Time Marching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Temporal Discretization}
In time-dependent problems, there needs to be an implementation of some time marching method. In general these methods start with an initial condition and approximate the time derivative using a numerical integration approximation. Time dependent problems can be written in standard form given by Equation \ref{eq:ODEstandard}. For this Thesis, the forcing term on the right hand side is the steady state residual as seen in Equation \ref{eq:reactorModelvec}.

\begin{equation}
\frac{\partial \vec{U}}{\partial t} = f(\vec{U}, t)
\label{eq:ODEstandard}
\end{equation}

The time dependent problems applicable to reactor analysis are usually stiff problems, which require an implicit type of time integrator to solve the problem with reasonable time step sizes. Runge-Kutta methods are chosen for the implementation of this Thesis because a general framework can be implemented that can handle multiple time integrators of various orders. Special focus is given to ``Singly Diagonally Implicit Runge-Kutta" (SDIRK) methods because the stages of these methods are able to be solved sequentially rather than concurrently in one system \cite{ODE-Butcher}.   The $Y$-formulation for a general $s$-stage Runge-Kutta Method will be defined as

\begin{equation}
	\begin{gathered}
	\vec{Y}_i = \vec{U}^n + \tau \sum_{j=1}^i a_{ij} f(\vec{Y}_j, t_n + c_j \tau), \qquad i = 1, 2, ..., s \\
  \vec{U}^{n+1} = \vec{U}^n + \tau \sum_{i=1}^s b_i f(\vec{Y}_i, t_n + c_i \tau),
	\end{gathered}
\label{eq:timeDiscretization}
\end{equation}

\noindent
where $\tau$ is the time step size and $\{a,b,c\}$ are given by the Butcher Tableaux characteristic of the Runge-Kutta method used.

In general, the sum index in the stage-$i$ residual ranges from 1 to $s$; then the entire classification of the Runge-Kutta method is dependent on the form of $A$. If $A$ is strictly lower triangular (non-zero for $i < j$), the Runge-Kutta method is explicit and a linear system can be solved at every stage. In the case of $A$ being lower triangular (non-zero for $i \leq j$), the method is said to be ``diagonally" implicit and a nonlinear solve is needed at every stage, but the stages can be computed sequentially. In the case that $A$ is full, the method is fully implicit and a nonlinear solve for all stages must be accomplished concurrently. Since this Thesis focuses on SDIRK methods, the sum index on the stage-$i$ is restricted to $j\in[1,i]$, and the Butcher Tableaux reflect this structure. A general Butcher Tableau has the form

\begin{center}
\begin{tabular}{c|c}
$c$    &     $A$ \\ \hline
           &     $b^T$ \\
\end{tabular}
\end{center}

\noindent
where the vectors $b,c$ are of size $s$ and the matrix $A$ is of size $s\times s$. An explanation of the Butcher Tableaux for the time integrators used in this thesis are provided in Appendix A.

\subsection{Transient Residual Formulation}

The transient residual is formed using Rothe's method of discretizing the time derivative and then discretizing the spatial domain. The treatment of spatial discretization is detailed in the next section. The formation of the transient residual before spatial discretization becomes Equation \ref{eq:trResidual}.

\begin{equation}
\label{eq:trResidual}
F(Y_i) = Y_i - \vec{U}^n + \tau \sum_{j=1}^i a_{ij} f(Y_j,t_n+c_j\tau), \qquad i=1,2,...,s
\end{equation}

After the solution of all stage residuals, a linear combination of the stage residuals is added to the current solution to yield the solution at the next time step Equation \ref{eq:NewSolution}.

\begin{equation}
\label{eq:NewSolution}
\vec{U}^{n+1} = \vec{U}^n + \tau \sum_{i=1}^s b_i f(Y_i, t_n+c_i\tau)
\end{equation}

\subsection{Low Order Runge-Kutta Methods}

The reason a general $s$-stage Runge-Kutta method is used in this Thesis is to provide a framework that allows a variety of time integrators to easily be implemented. The restriction imposed is that the method needs to be diagonally implicit. Various Butcher Tableaux implemented in this Thesis are given in Appendix A. The convergence order of some methods are shown in Table \ref{tab:RK}.

\begin{table}[H]
\centering
\caption{Outline of Time Marching Methods}
\begin{tabular}{|c|c|}
\hline
Backward Euler (BE) & $\mathcal{O}(\tau)$ \\ \hline
Crank-Nicholson (CN) & $\mathcal{O}(\tau^2)$ \\ \hline
SDIRK22 & $\mathcal{O}(\tau^2)$ \\ \hline
SDIRK33 & $\mathcal{O}(\tau^3)$ \\ \hline
\end{tabular}
\label{tab:RK}
\end{table}

While higher order methods could easily be implemented, the goal of this aspect of the Thesis is to outline the framework for which additional methods could be used. To implement higher order Runge-Kutta methods, one simply needs to input the Butcher Tableau characteristic for that method.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%              Section III --- Spatial Discretization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Spatial Discretization}
\label{Spatial Discretization}

\subsection{Continuous Galerkin FEM}
The Finite Element Method has been used to numerically solve many types of partial differential equations \cite{Segerlind1984}. The method involves transforming the PDEs into their respective weakforms by multiplying by a test function, denoted by $b_i$, and integrating over the entire domain $\Omega$. The solution can then be represented as an infinite series of shape functions given by

\begin{equation}
U(\vec{x}) = \sum_{j=1}^\infty b_j(\vec{x}) U_j,
\label{eq:soln-rep}
\end{equation}

\noindent
where the \{$U_j$\} are coefficients of the expansion. No approximations have been made thus far, and if the coefficients \{$U_j$\} could be computed, the exact solution would be produced. It is unrealistic to solve for an infinite amount of unknowns on a finite precision machine and so a truncated sum approximation is made.

\begin{equation}
U(\vec{x}) = \sum_{j=1}^N b_j(\vec{x}) U_j
\label{eq:soln-approx}
\end{equation}

The next choice to make is choosing the form of the shape functions and test functions. In the Continuous Galerkin Method, the shape functions and test functions are chosen to be the same \cite{Segerlind1984}; in general these functions are chosen to be local polynomials over a mesh cell. This means that these functions are only non-zero over a few cells in the mesh and zero everywhere else. An example of the first three orders of such functions over the unit cell in one dimension \cite{Guermond2004} are given by Figure \ref{fig:basis-functions}.

\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/LinearElementBases.png}
    \caption{Linear Order}
    \label{fig:Q1-Element}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/QuadraticElementBases-Nodal.png}
    \caption{Quadratic Order}
    \label{fig:Q2-Element}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/CubicElementBases-Nodal.png}
    \caption{Cubic Order}
    \label{fig:Q3-Element}
  \end{subfigure}
\caption{Lagrangian finite element basis functions on the unit cell in one dimension for three polynomial orders}
\label{fig:basis-functions}
\end{figure}

Regardless of the basis functions used, the mathematics for the finite element method are the same. It is illustrative to take a simple example such as a simple diffusion equation in a linear system.

\subsection{Discretization Formulation}

The Diffusion equation is given by Equation \ref{eq:diffusion}; it is simply the Laplacian of the solution with an interaction term and a forcing term.

\begin{equation}
- \nabla \cdot D(\vec{x}) \nabla U(\vec{x}) + \Sigma_t (\vec{x}) U(\vec{x}) = q(\vec{x}) \qquad \vec{x} \in \Omega
\label{eq:diffusion}
\end{equation}

Equation \ref{eq:diffusion} can be transformed to its weak form by multiplying by a test function and integrating over the domain $\Omega$,

\begin{equation}
- \int_{\Omega} d\vec{x} \, b_i(\vec{x}) \nabla \cdot D(\vec{x}) \nabla U(\vec{x}) + \int_{\Omega} d\vec{x} \, b_i(\vec{x}) \Sigma_t(\vec{x}) U(\vec{x}) = \int_{\Omega} d\vec{x} \, b_i(\vec{x}) q(\vec{x}) \nonumber
\label{eq:wf-integral}
\end{equation}
\begin{equation}
- \left( b_i(\vec{x}), \nabla \cdot D(\vec{x}) \nabla U(\vec{x}) \right)_{\Omega} + \left( b_i(\vec{x}), \Sigma_t(\vec{x}) U(\vec{x}) \right)_{\Omega} = \left( b_i(\vec{x}), q(\vec{x}) \right)_{\Omega}
\label{eq:wf-innerProduct}
\end{equation}

Here the convenient inner product notation is used to denote integration over the domain. The first term of Equation \ref{eq:wf-innerProduct} can be integrated by parts to transform the Laplacian operator to a gradient operator (the test function's spatial dependence designation is removed from Equation \ref{eq:wf-boundaryTerm} to write the equation more compactly).

\begin{equation}
\left( \nabla b_i, D(\vec{x}) \nabla U(\vec{x}) \right)_{\Omega} - \left(  b_i \vec{n}, D(\vec{x}) \nabla U(\vec{x}) \right)_{\partial\Omega} + \left( b_i, \Sigma_t(\vec{x}) U(\vec{x}) \right)_{\Omega} = \left( b_i, q(\vec{x}) \right)_{\Omega}
\label{eq:wf-boundaryTerm}
\end{equation}

The boundary term is generally carried through the derivation and will influence the solution. However in the applications discussed in this Thesis, the boundary conditions are either imposed (Dirichlet) or the gradient of the solution is zero (Reflecting); thus the boundary term will evaluate to zero in either case and will be dropped from this example.

The linear combination of basis functions approximation for the solution is inserted into the weakform. The terms of the weakform can be represented by dot products with the unknown coefficients and can be transformed into a linear system.

\begin{equation}
A\vec{U} = \vec{Q}
\label{eq:linearSystem}
\end{equation}

Here $A_{ij} = \left( \nabla b_i(\vec{x}), D(\vec{x}) \nabla b_j(\vec{x}) \right)_{\Omega} + \left( b_i(\vec{x}), \Sigma_t(\vec{x}) b_j(\vec{x}) \right)_{\Omega}$ and $Q_i = \left( b_i(\vec{x}), q(\vec{x}) \right)_{\Omega}$. These terms show up frequently in the finite element method and thus are given special names. The product of the gradient of two basis functions integrated over the domain is called the ``stiffness matrix", and is denoted ``$K_D$". The product of two basis functions integrated over the domain is called the ``mass matrix", and is denoted by ``$M_\Sigma$". The subscripts on the special matrices denote the dependence on spatially varying coefficients. This equation can be solved by inverting the matrix $A = K_D+M_\Sigma$. In this way, the mass and stiffness matrices can be computed for any order of basis functions and the resulting system is similar apart from its size and the values of the entries.

In practice, the integration over the domain cannot be carried out analytically and must be approximated by numerical quadrature. In addition, the domain is broken into a spatial mesh, denoted $\mathcal{T}$, which contains non-overlapping cells that cover the entire domain; in the Deal.ii library, the cells are regular quadrilaterals in 2D and hexahedra in 3D. The integration over the entire domain can be broken into a sum of integrations over cells. For instance to evaluate the $A_{ij}$ matrix, the following transformation is performed.

\begin{equation}
A_{ij} = \sum_{K\in\mathcal{T}} \int_K d\vec{x} \ \nabla b_i(\vec{x}) D(\vec{x}) \nabla b_j(\vec{x}) + b_i(\vec{x}) \Sigma_t(\vec{x}) b_j(\vec{x})
\label{eq:sumOverCells}
\end{equation}

To proceed further however, a numerical approximation must be made to perform the integral over a cell; this numerical approximation is known as quadrature integration the details of which do not add to the understanding of this topic.

\subsection{Coupled Physics}

This Thesis is oriented towards solving coupled physics problems which can add complexities to spatial discretization. Taking the steady state residual from Equation \ref{eq:reactorModel} and applying the techniques developed in the previous section produces a discretized steady state residual Equation \ref{eq:ssResidual}.

\begin{equation}
\label{eq:ssResidual}
F^{SS} = 
\begin{bmatrix}
 K^{\phi \phi}_D+M^{\phi \phi}_\Sigma & 0 \\
 M^{T \phi}_\kappa & K^{T T}_k \\
 \end{bmatrix}
 \left[ \begin{array}{c} \phi \\ T \\ \end{array} \right]
 + \left[ \begin{array}{c} Q_{\phi} \\ Q_{T} \end{array} \right]
\end{equation}

Here the diagonal entries are representative of what was covered in the previous section. The superscript notation helps distinguish the use of multiple solution meshes. For instance the matrix $K_D^{\phi\phi}$ is the stiffness matrix assembled with basis functions from only the $\phi$ mesh. Whereas the matrix $M_\kappa^{T\phi}$ is the mass matrix that is assembled using basis functions from the $T$ and $\phi$ meshes. The off-diagonal terms are produced in the same way that the diagonal terms are produced, except that the test function and shape function are defined over independent meshes. If the two variables have the same spatial mesh, this detail is inconsequential. However, if the two variables have different spatial meshes, special care must be taken when assembling these terms; this situation is discussed in section \ref{Coupled Physics}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section IV --- Spatial Adaptivity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Spatial Adaptivity}
\label{sec:SpatialAdaptivity}

\subsection{Motivation}

It can be shown that the convergence of the L$^2$ norm solution error ($\|u-u_h\|_{L^2}$) is dependent on the size of mesh cells used to approximate the solution with order $\mathcal{O}(h^{p+1})$, where $h$ is the cell size and $p$ is the order of polynomial approximation \cite{FEM-Zienkewicz}. Equation \ref{eq:globalRefinement-h} predicts the asymptotic error in the solution when compared to the spatial mesh size. It is sometimes more convenient to express this reduction in error in terms of the number of unknowns in the solution vector; Equation \ref{eq:globalRefinement-dof} describes the error convergence in this sense.

\begin{equation}
\varepsilon \propto h^{p+1}
\label{eq:globalRefinement-h}
\end{equation}
\begin{equation}
\varepsilon \propto (N_{dofs})^{-\frac{p+1}{d}}
\label{eq:globalRefinement-dof}
\end{equation}

Equations \ref{eq:globalRefinement-h} \& \ref{eq:globalRefinement-dof} describe how the solution error will behave when the domain is refined uniformly. This type of refinement can yield very accurate results, but in most cases the computational load becomes prohibitive. Thus uniform refinement is reserved for diagnostic purposes to determine whether the numerical methods have been implemented correctly.

In general spatial refinement is needed in places where the local solution error is large. An error estimator can be used to locate where local solution error resides in the spatial domain; the exact error estimator for the Laplacian operator is known as the Kelly Error Estimator. This estimator determines the jump of the solution gradient across mesh cell boundaries \cite{Kelly-error}. This estimator is not an exact estimator for the equations studied in this Thesis, but the goal is not to know the error to a high accuracy only to locate those cells where the error is large compared to other locations in the domain. Thus the Kelly Error Estimator will be referred to as an error indicator in this sense because it cannot estimate the size of the numerical error, but only where the error is localized. The rule of thumb is that where the gradient is large, the spatial error is also large; thus refinement is likely to be needed where there are localized features. 

The type of AMR studied in this Thesis is h-AMR where the ``h" refers to the spatial mesh size. Other types of AMR involve varying the polynomial approximation degree and are given the letter ``p" (p-AMR, hp-AMR) \cite{FEM-Zienkewicz}. While these additional types of AMR could yield more accurate results, this Thesis concentrates on h-AMR since the results can easily include hp-AMR, if available. In all cases where AMR is used, the goal is to solve the problem at hand with the same precision from using a uniformly refined mesh with a lower computational budget. The savings in computational budget can either be used to solve the problem faster or more precisely. 

\subsection{Coupled Physics}
\label{Coupled Physics}

This Thesis is concerned with solving coupled physics problems where each physics component potentially has different properties (i.e., local features in different parts of the domain). From the discussion in the previous section, it can be postulated that a more optimal setting would have each physics component defined on an independent mesh. This means that each solution component can be free to have an optimized mesh that is independent of other variables. 

A point that is necessary to discuss at this time is the implication of using Multimesh AMR on coupled physics problems. This technique may introduce issues if care is not taken to overcome them. In the coupling term for $\phi$ and $T$, the residual contains the following integral (Equation \ref{eq:coupledIntegral}).

\begin{equation}
\left[ \int_\Omega b_i^{\phi} \kappa b_j^{T}\right] \phi
\label{eq:coupledIntegral}
\end{equation}

When implementing these types of integrals in finite element software, this type of integral is transformed into a sum of integrals over the cells that make up the domain (as shown in Equation \ref{eq:sumOverCells}). This transformation is no longer possible since the basis functions for each variable is defined over different cells, i.e, $b_i^{\phi}$ is defined on $\mathcal{T}_\phi$ and $b_j^T$ is defined on $\mathcal{T}_T$. The first observation of how to avoid this conundrum was made in the paper by Yaqi Wang \cite{h-Yaqi}. In this paper, the authors introduce an algorithm to assemble the discrete version of Equation \ref{eq:coupledIntegral} based on the assumptions of:


\begin{enumerate}
\item All independent meshes are derived from the same initial coarse mesh
\vspace{-10pt}
\item Refinement is achieved by regular bisection of cells
\vspace{-10pt}
\item Embedded finite element spaces are used where each basis function on a cell can be represented as a linear combination of basis functions on the next refined cell.
\end{enumerate}


Under the first two assumptions the meshes for each variable, though independent, are connected; there can always be a mesh found that is the intersection of the two independent meshes, denoted $\mathcal{T}_\phi \cap \mathcal{T}_T$. Thus the integral in Equation \ref{eq:coupledIntegral} can be written as Equation \ref{eq:intersectionIntegral}.

\begin{equation}
\left[ \sum_{K\in\mathcal{T}_\phi \cap \mathcal{T}_T} \int_K b_i^{\phi} \kappa b_j^{T}\right] \phi
\label{eq:intersectionIntegral}
\end{equation}

For the intersection mesh $\mathcal{T}_\phi \cap \mathcal{T}_T$, at least one of the basis functions, $b_i^\phi$ or $b_j^T$, must be defined on the cell $K$, but the other could be defined on parents of the cell $K$. The terms ``parent" and ``child" cell are used to convey the way adapted meshes are produced; each cell from both meshes starts from the same state and, when refined, produces child cells which can be further refined. Since embedded finite element spaces are used on each cell, the parent cell's basis functions can be represented as a linear combination of the child cell's basis functions. 

Assume that a cell in $\mathcal{T}_T$ is once more refined than a cell in $\mathcal{T}_\phi$; the cell in $\mathcal{T}_\phi$ ($K_p$) is a parent of the cells in $\mathcal{T}_T$ ($K_c$). Thus the basis functions of $K_p$ need to be represented on $K_c$ to complete the transformation in Equation \ref{eq:intersectionIntegral}. Equation \ref{eq:basisInterpolation} shows that a matrix can be constructed to interpolate basis functions defined on $K_p$ to $K_c$.

\begin{equation}
b_i^\phi |_{K_c} = B_c^{il} b_l^T |_{K_c}
\label{eq:basisInterpolation}
\end{equation}

The matrix $B_c^{il}$ interpolates data from a parent cell to its $c^{th}$ child cell. Since a child of cell $K_p$ can have children of its own, this process can be computed recursively until the cell on the intersection mesh ($K$) is reached. When all basis functions have representations on the cells of the intersection mesh, the integral over a cell $K$ is evaluated using numerical quadrature.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%               Section V --- Dynamic Mesh Refinement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dynamic Mesh Refinement}
A consequence of using AMR for time dependent problems is that the spatial mesh may need to change in time. Previous publications on dynamic AMR \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-DynamicMesh} have performed mesh refinement at every time step. This strategy can become overly costly if the solution is not changing rapidly in time. Additionally, these algorithms start spatial refinement for each time step from the initial coarse mesh; this method is mathematically clean (each spatial mesh has no influence from previous meshes), but can become costly. For this Thesis, a modified version of the algorithm in \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-DynamicMesh} is implemented where each time step starts spatial adaptivity on the mesh from the previous time step. An additional feature is then implemented where the same mesh can be used for multiple time steps if the solution is not changing significantly. This added feature, referred to as the ``Propagating Mesh", is designed to further balance the need for accurate solutions and shorter compute times.

\subsection{Adaptivity at Every Time Step}
A version of the algorithm presented in \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-DynamicMesh} is implemented in this Thesis, where the mesh from the previous time step is used as the starting mesh for the current time step's AMR process. The solution used to test this algorithm has a steep gradient and will force the cells around the solution to become small, as shown in Figure \ref{fig:wakeSolution}. To prevent either component of the solution error (spatial or temporal) from dominating, the two discretization sizes should remain on the same order ($h \sim \tau^\frac{s}{p+1}$). However, by utilizing this constraint, the decreasing mesh size would drive the time step size smaller and would eventually halt the simulation. The algorithm developed for this Thesis uses equally sized time steps and does not adhere to this constraint. Consequently, if the solution is forced to move past small spatial cells, oscillations can occur; this is observed in Figure \ref{fig:wakeSolution} as the refinement ``wake" in the solution's path.

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/wake01.png}
	\caption{t = 0s}
	\label{fig:wake1}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/wake02.png}
	\caption{t = 0.75s}
	\label{fig:wake2}
	\end{subfigure}
\caption{Unlimited spatial mesh size under dynamic AMR}
\label{fig:wakeSolution}
\end{figure}

To prevent the type of refinement behavior in Figure \ref{fig:wakeSolution}, a limit on the mesh size is introduced. The mesh size control is implemented by not allowing a cell to be refined further than a set  maximum level of refinement. The maximum level of refinement is chosen to be the same as the initial level of refinement input by the user; the user specifies how much to refine the initial mesh. The justification for this choice stems from the observation that any error in the initial condition will propagate through the time evolution. Thus, having a fine mesh on the final solution is not helpful unless the initial condition had the same level of spatial resolution.

The implementation of the spatial mesh size limit may not be the best for general problems, but is shown to work well with the problems tested. A more robust method would base the mesh size limiter on the time step size. This type of mesh size limit is discussed in the section on future work. The next section discusses the feature added to the dynamic AMR algorithm.


\subsection{Propagating Mesh Feature}
\label{sec:propagatingMesh}

An extension of the algorithm used in \cite{Dubcova-MicrowaveHeating,hp-Coupled,Solin-DynamicMesh} can be to use the same spatial mesh over multiple time steps when the solution is slowly varying in time. To implement this extension, one needs some way to quantify the change of a solution in time and be able to judge when and where to spatially adapt. The methods described in this section address these concerns and utilize the mesh size control feature from the previous section.

During the spatial adaptation process, an indicator for the solution error is obtained on a cell-wise basis. This indicator forms a vector in $\mathbb{R}^n$ where $n$ is the number of mesh cells. If the solution does not change in time, this vector is stationary; as the solution changes in time, this vector will deviate from its original state. The angle between the original vector and the current vector can be computed using the dot product (Equation \ref{eq:dotProduct}). When the angle between the two vectors is large enough, the solution is said to have moved significantly and the spatial mesh needs to be adapted again.

\begin{equation}
\theta_i = \cos^{-1} \frac{V_1 \cdot V_i}{ \|V_1\| \|V_i\|} \leq \theta_\mathrm{tol}
\label{eq:dotProduct}
\end{equation} 

Here $V_1$ \& $V_i$ are the error vectors after the last spatial refinement and at the current time, respectively; the index ``$i$" denotes the current time step. The user can specify the acceptable angle ($\theta_\mathrm{tol}$) that triggers spatial refinement (usually between 1$^\circ$ - 45$^\circ$). A schematic of this process is shown in Figure \ref{fig:timeDomain} where a given adapted spatial mesh is re-used over several time steps from time $t_1$ to time $t_{1+\alpha}$, with $\alpha$ denoting the number of times Equation \ref{eq:dotProduct} was satisfied.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{figures/timeDomain.png}
\caption{Time domain schematic when a spatial mesh is used for multiple time steps}
\label{fig:timeDomain}
\end{figure}

Figure \ref{fig:timeDomain} shows that the same spatial mesh is used for $t\in[t_1,t_{1+\alpha}]$. In the schematic, ``$i$" represents the current time step. Consequently, the parameter $\alpha$ is not known \emph{a priori}, but only when $\theta > \theta_\mathrm{tol}$ by using the vectors $V_1$ \& $V_{1+\alpha}$. The vectors \{$V_i$\} are stored, and when the spatial mesh needs refinement these vectors are averaged (Equation \ref{eq:averageError}) to produce an error indicator for where the mesh should be adapted.

\begin{equation}
\overline{V} = \frac{1}{1+\alpha} \sum_{i=1}^{1+\alpha} V_i
\label{eq:averageError}
\end{equation}

The justification for determining the error indicator in this way comes from the idea that the spatial refinement should reflect any changes in the solution that happened during the time while the mesh was fixed. Averaging the error indicators over this time interval will produce a gross estimate of how the solution behaved in the interval $[t_1,t_{1+\alpha}]$.

An extension to the Propagating Mesh could be to restart the time evolution after each spatial adaptation. From Figure \ref{fig:timeDomain}, when the solution reaches time $t_{1+\alpha}$, the mesh would be spatially adapted based on the error indicator in Equation \ref{eq:averageError} and the solution process restarted from time $t_1$. This extension was briefly studied during this Thesis and will be discussed in the future work section.

The solution techniques discussed in this chapter were implemented in code, using namely the Deal.ii Finite Element Library \cite{BangerthHartmannKanschat2007,DealIIReference}. The Deal.ii Library is a finite element library written in C++, widely used in academia and industry, and offers a high quality environment for the rapid development of software based on adaptive finite elements. Each method that is implemented in code needs to be tested to ensure that a mathematically correct answer is produced from the code. The next chapter discusses the ways to test the methods presented in this chapter, along with the testing results.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           Chapter IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\chapter{\uppercase{Code Verification}}

In any application where a model of a physical process is conducted, the question always in mind is ``How close is the result to reality?" This question can be broken into two other distinct questions, namely ``How accurately does the model approximate reality?" and ``How accurately does the implementation solve the model?" The first question is addressed through code validation, or the testing of a model with experiments to determine how closely the model can predict the experimental results \cite{Oberkampf-VV}. Code validation can be an arduous task that involves a high amount of development and resources; this Thesis will not be concerned with the validation of the models employed here. However, the second question of code verification, or the testing of the implementation of the model, is addressed. Code verification requires thought and planning, but is a more manageable goal when compared with code validation.

The process of code verification finds a way to compare an exact solution to a computed solution. One way this can be achieved is to transform the problem under consideration into a simpler problem --- one for which an exact solution exists. For example, the material properties could be made constant throughout the domain and observe how the computed solution behaves. Unfortunately, when making the problem simpler, one is not testing the problem in its most general sense and implementation errors can be overlooked. A more robust process would produce an exact solution without making the problem simpler. One such method, used in this Thesis, is known as the Method of Manufactured Solutions \cite{Roache-MMS}.

The Method of Manufactured Solutions (MMS) involves specifying an exact solution that will have interesting features to test. This solution is input into the undiscretized governing equation and produces a residual, since it is unlikely that the exact solution will satisfy the original equation. This residual is added as a forcing term in the governing equation so that the specified exact solution will satisfy the equation. There are many advantages to this method; the most distinguished is that the problem being solved does not need to be modified, except for adding a forcing term.

In the following sections, several manufactured solutions are used. The problem parameters common to all manufactured solutions are given in Table \ref{tab:MMSparams} and the nonlinear form of the neutron absorption coefficient is given by Equation \ref{eq:sigmaA}. In all of the cases, the domain length in all directions is $L = 10$.

\begin{table}[H]
\centering
\caption{Problem Parameters Common to All Manufactured Solutions}
\begin{tabular}{c c|c c||c c|c c}
$\phi$  & value & T & value & $\phi$  & value & T & value\\ \hline
$\Sigma_a^0$ & 3.0 & $\gamma$ & 0.001 & $C_\phi$ & 2.5 & $C_T$ & 1.6 \\ 
$\nu(1-\beta)$ & 1.44 & $T_\mathrm{ref}$ & 3.0 & $x_0$ & 5.0 & $x_0$ & -5.0 \\ 
$\Sigma_f$ & 5.0 & $\kappa$ & 0.001 & $y_0$ & 5.0 & $y_0$ & -5.0 \\ 
D & 2.0 & k & 1.0 & $\sigma_\phi$ & 0.1 & $\sigma_T$ & 0.1 \\ 
\end{tabular}
\label{tab:MMSparams}
\end{table}

\begin{equation}
\Sigma_a(T) = \Sigma_a^0 \left[ \gamma ( T^2 - T_\mathrm{ref}^2 ) \right]
\label{eq:sigmaA}
\end{equation}

For the types of problems tested in the following sections, the material properties ($\Sigma_a, D, \kappa, \gamma$, etc.) are inconsequential since they are used to produce the residual for MMS. The parameters that determine the solution behavior are the manufactured solution parameters ($C_\phi, C_T, x_0, y_0$, etc.).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Verification of Spatial Discretization}

A good starting point for code verification is to determine whether the steady state problem is being solved correctly. Rothe's method is used to discretize the temporal domain, which is essentially solving a steady state problem at every time step \cite{Blank-Rothe}. Thus, if the steady state problem is not being solved correctly, there is no hope that the transient problem will be solved correctly. 

The metric used to determine whether the spatial solution is being solve correctly or not is to compare the solution error ($\|u-u_h\|_{L^2}$) to the mesh size. In Section \ref{sec:SpatialAdaptivity}, Equation \ref{eq:globalRefinement-h} showed that the error decreased at a prescribed rate as the spatial mesh size decreased. Thus the solution error will be computed for meshes with a decreasing cell size, and the solution error should trend as in Equation \ref{eq:globalRefinement-h} after a sufficiently small mesh size is reached.

The choice of solution is an important aspect of MMS. It is desirable to have a solution that cannot be exactly represented using a finite polynomial; since the approximation space is spanned by polynomial functions. If the same degree of polynomial is used for the approximation space as the exact solution, the spatial error will be close to machine precision and spatial refinement would not further decrease the solution error. Thus an exact solution to test spatial convergence could be of a trigonometric or exponential form in space, for instance.

The proposed manufactured solution to test the spatial convergence is a ``stationary Gaussian peak". The functional form is given by Equation \ref{eq:MMS-Stationary-Gauss} where the exponential provides the Gaussian peak, and the quadratic term specifies the Dirichlet boundary values. This equation describes the solution to a single solution component; each solution component can have different peak positions, amplitudes, and peak widths. A visual representation of one component of the solution is shown in Figure \ref{fig:MMS-Stationary-Gaussian}.

\begin{equation}
U(\vec{x}) = C_u \prod_{i=1}^{d} \left[ 1 - \left( \frac{x_i}{L} \right)^2 \right] e^{-\frac{\left(x_i - x_i^{0,u} \right)^2}{\sigma_u} }
\label{eq:MMS-Stationary-Gauss}
\end{equation}

The variable $x_i$ denotes a spatial coordinate, while $x_i^{0,u}$ denotes the location of the peak for component ``u". In this manufactured solution the parameter $x^0$ does not change in time, making the peak stationary.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{figures/MMS-Solution-Stationary-Gaussian.png}
\caption{Stationary Gaussian manufactured solution for one of the two solution components. Solution for $\phi$ is shown.}
\label{fig:MMS-Stationary-Gaussian}
\end{figure}

The rate of decrease in the solution error depends on both the mesh size and the order of the polynomial approximation, as was seen in Equation \ref{eq:globalRefinement-h}. In Figure \ref{fig:globalRefinement}, the solution error is evaluated for decreasing mesh sizes; the computation is repeated for Q1, Q2, and Q3 elements. The slopes denoted in the figure are the expected convergence rates from Equation \ref{eq:globalRefinement-h}.

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Phi1_global_h.png}
	\caption{$\phi$}
	\label{fig:phi1spatial}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Phi2_global_h.png}
	\caption{$T$}
	\label{fig:phi2spatial}
	\end{subfigure}
\caption{Spatial convergence for uniform mesh refinement}
\label{fig:globalRefinement}
\end{figure}

Observe that as the spatial mesh size decreases, the solution error also decreases at the predicted rate after a sufficiently small mesh size. When the mesh size is still large ($\mathcal{O}(1)$) the solution error does not yet decrease at the predicted rate. This can be attributed to the spatial mesh being to coarse to accurately represent the solution and thus the convergence rate only holds if the spatial mesh represents the solution well enough. In slightly different words: the convergence rate asymptotically approaches predicted convergence rate as $h\to0$. The solution error appears to decrease according to the predicted rate, and thus the spatial discretization is declared to be implemented correctly.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Verification of Temporal Discretization}

Once the steady state problem is determined to be implemented correctly, the transient problem can be addressed. In the same way as the spatial component, it is desirable to isolate the time portion of the solution. An excellent choice for a manufactured solution to test the transient problem would have very little spatial error so that the majority of the solution error was produced by the time integrator. In the previous section it was noted that if the manufactured solution is a polynomial in space and the approximation finite element space spans polynomials of at most the same degree, then the spatial error would be close to machine precision; this is the type of behavior desired here to isolate the temporal error.

To test the time integrator, the manufactured solution's spatial component is chosen to be some polynomial with the same degree as the approximation space. The time portion of the manufactured solution makes the solution change in some way that cannot be represented as a polynomial, so that the temporal behavior cannot be resolved exactly by the time integrator. Various time integrator methods are tested that were included in Table \ref{tab:RK}, each having a specific convergence rate. The proposed manufactured solution is given by Equation \ref{eq:MMS-Quadratic} where the solution is a quadratic ``bubble" function in space with a temporal amplitude changing as a sine wave. Each solution component's amplitude changes at a different rate ($\omega_\phi = 1.5, \omega_T = 0.2$).

\begin{equation}
U(\vec{x},t) = C_u \left[ \sin(\omega_u t) + 1 \right] \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L} \right)^2  \right]
\label{eq:MMS-Quadratic}
\end{equation}

The amplitude can vary between zero and two; in the simulations presented the end time is chosen to be less than $\frac{3\pi}{2}$ so that the solution is always non-zero. The solution being zero should not detract from the results obtained since there is nothing in the models that prevent a variable from being zero.

To determine whether the time integrator is working properly, much like in the previous section, the transient problem is computed with various sized time steps and the solution error should converge at a rate proportional to the time integrator order. The test is set up so that the spatial approximation uses Q2 elements that will resolve the spatial component to within machine precision, and the time interval is $t\in[0,1s]$. Since the time interval is fixed, as the number of time steps increases the size of the time step decreases; in fact there is a one-to-one relationship between the number of time steps and the time step size. 

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Phi1_time.png}
	\caption{$\phi$}
	\label{fig:phi1time}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Phi2_time.png}
	\caption{$T$}
	\label{fig:phi2time}
	\end{subfigure}
\caption{Temporal convergence for several time integrator methods}
\label{fig:temporalRefinement}
\end{figure}

Figure \ref{fig:temporalRefinement} shows the solution error compared to the number of time steps taken. Four time integrator methods were tested that give convergence rates ranging from linear to cubic. For both variables, the solution error decreases at the predicted rate and thus it seems that the temporal portion of the implementation is correct.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Connection between Space and Time}

In reality, the spatial error and the temporal error both contribute to the overall solution error. In general the overall solution error will converge as $\mathcal{O}(h^{p+1}+\tau^s)$, where $h$ and $\tau$ are the spatial mesh size and the temporal step size respectively. Both parameters $\{h,\tau\}$ must be controlled relative to each other; if one is large compared to the other, that error will dominate the solution error. In the previous sections, the size control was addressed by only allowing a specified level of refinement for a cell. A reasonable approach would be to limit the spatial mesh size as $h \sim \tau^\frac{s}{p+1}$, since the time step size is fixed in these simulations. 

To show that the spatial mesh size and the time step size are connected, a manufactured solution is devised that cannot be resolved by a discrete polynomial approximation in either space or time. The proposed solution has a smooth and quadratic initial condition much like the solution described in the time verification section. After the simulation starts, a Gaussian peak grows into the solution. The height of the Gaussian peak grows as a Maxwellian in time so that the initial height is zero and grows to a maximum and then decays exponentially. The form of the manufactured solution is given by Equation \ref{eq:MMS-Maxwellian}. The behavior of this solution is similar to what would happen in a nuclear reactor during a rod ejection accident; the initial flux is smooth, and when the accident occurs a local spike in the flux appears and then the entire solution renormalizes. Because of the similarity to a reactor accident, this solution will be referred to as the ``Reactor" solution in this section. In each of the solution components, the peak is stationary ($\omega_{\phi,T} = 0.0$) and the growth factor is moderate ($\alpha_{\phi,T}=1.0$).

\begin{equation}
U(\vec{x},t) = C_u \left[ \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L}  \right)^2 \right] + \frac{\alpha_u}{\sigma_u} t e^{-\alpha_u t} \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L}  \right)^2 \right] e^{- \frac{\left( x_i - x_i^{0,u} \right)^2}{\sigma_u} } \right]
\label{eq:MMS-Maxwellian}
\end{equation}

If $\alpha_u$ is made large, the peak will appear and disappear rapidly; also if $\sigma_u$ is made small, the peak will occupy a small region of the domain. The constant, $\frac{\alpha_u}{\sigma_u}$, ensures that the height of the peak will be much greater ($\sim 10\times$) than the maximum of the initial condition. A graphical representation of the manufactured solution, described by Equation \ref{eq:MMS-Maxwellian}, is shown in Figure \ref{fig:MMS-Maxwellian}. After the simulation begins the localized peak quickly grows, reaches a maximum, and then decays away exponentially.

\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/MMS-Solution-Maxwellian.png}
    \caption{Manufactured solution visual representation}
    \label{fig:MMS-Maxwellian-space}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/Maxwellian-Yaxis.png}
    \caption{Time behavior of solution at peak location (5,0)}
    \label{fig:MMS-Maxwellian-time}
  \end{subfigure}
\caption{Manufactured solution with smooth initial condition and appearing peak. Solution for $\phi$ is shown.}
\label{fig:MMS-Maxwellian}
\end{figure}

Figure \ref{fig:space-time-BE} shows the convergence of the solution when using Q2 spatial elements and the Implicit Euler time integrator with $t\in[0,1s]$. Figure \ref{fig:spacetime-space-BE} shows how the solution error decreases as the spatial grid is refined for different numbers of time steps. It is seen that for a low number of time steps, the spatial convergence plateaus; this is caused from the time integrator error dominating the solution error. As the number of time steps increases and the temporal error no longer dominates, the theoretical spatial convergence rate is approached. Likewise Figure \ref{fig:spacetime-time-BE} shows the temporal convergence for differing levels of spatial refinement. The same effect is observed in that if the spatial resolution is coarse, the temporal convergence rate plateaus due to the spatial error dominating. As the mesh is made finer and the spatial error no longer dominates, the temporal convergence approaches the theoretical rate.

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Space_BE_Q2.png}
	\caption{Spatial convergence}
	\label{fig:spacetime-space-BE}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Time_BE_Q2.png}
	\caption{Temporal convergence}
	\label{fig:spacetime-time-BE}
	\end{subfigure}
\caption{Convergence with space and time using BE time integrator and Q2 elements}
\label{fig:space-time-BE}
\end{figure}

Figure \ref{fig:space-time-33} shows the convergence for the same problem, but instead of Implicit Euler an SDIRK33 time integrator is used. Since this time integrator is a higher order, it is expected that the temporal component of the solution will be more resolved than is the case of a low order method with the same time step size.

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Space_SDIRK33_Q2.png}
	\caption{Spatial convergence}
	\label{fig:spacetime-space-33}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/Time_SDIRK33_Q2.png}
	\caption{Temporal convergence}
	\label{fig:spacetime-time-33}
	\end{subfigure}
\caption{Convergence with space and time using SDIRK33 time integrator and Q2 elements}
\label{fig:space-time-33}
\end{figure}

Comparing Figure \ref{fig:spacetime-space-BE} and Figure \ref{fig:spacetime-space-33} shows that the spatial convergence rate can be achieved in many fewer time steps when a higher order time integrator is used. The highlighted points in Figures \ref{fig:spacetime-space-BE} \& \ref{fig:spacetime-space-33} correspond to the same spatial and temporal discretizations. Notice that in Figure \ref{fig:spacetime-space-BE}, the solution error only decreases slightly between the two points. Conversely in Figure \ref{fig:spacetime-space-33}, the solution error decreases at the predicted rate. This result appeals to intuition because as the time integrator order increases, the temporal error will be reduced and the spatial error convergence rate should approach the result presented in Figure \ref{fig:globalRefinement}.

Comparing Figure \ref{fig:spacetime-time-BE} and Figure \ref{fig:spacetime-time-33} shows that a much more refined mesh must be used to obtain the correct temporal convergence rate. Again since the temporal error is reduced by increasing the integrator order, the spatial resolution must increase so that it will not dominate the solution error; this is exactly what is observed in transitioning from Figure \ref{fig:spacetime-time-BE} to Figure \ref{fig:spacetime-time-33}. In order to produce the correct temporal convergence rate, the spatial resolution must be increased. The goal of this discussion was to emphasize the importance of requiring a connection between the spatial mesh size and the temporal step size in the form of a discretization size limit as discussed in Section \ref{sec:propagatingMesh}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Properties of Propagating Mesh}

The propagating mesh feature was tested in two ways by manufactured solutions that had different properties. The first solution is given by Equation \ref{eq:MMS-Stationary-Gauss} except that the Gaussian peak is rotating around the origin so that $x^0$ is given by Equation \ref{eq:movingPeak}. The values of the rotation speed for each solution component is the same as the temporal discretization verification ($\omega_\phi=1.5,\omega_T=0.2$). This solution tests the propagating mesh algorithm by having the peak present in the initial condition so that the initial spatial mesh will be refined in a concentrated region of the domain. Thus, the spatial mesh will be required to travel along with the Gaussian peak during the time evolution.

\begin{equation}
x^{0,u}(t) = 
\begin{bmatrix}
sin(\omega_u t) \\
cos(\omega_u t) \\
0 \\
\end{bmatrix}
\label{eq:movingPeak}
\end{equation}

The second solution is given by Equation \ref{eq:MMS-Maxwellian} again. This solution is initially smooth an thus the refinement is scattered throughout the domain; a Gaussian peak emerges from the smooth solution and decays away. The propagating mesh algorithm should concentrate refinement around the peak even if refinement is not present in the initial condition.

Figure \ref{fig:revolvingGaussian} shows four snapshots in the evolution of the first solution referred to as the ``Traveling Gaussian" solution. Several observations can be made about how the algorithm handles this type of solution. First, Figure \ref{fig:movingGauss1} shows the initial condition for this solution. The mesh is refined in a concentrated region around the peak and is relatively coarse in the other parts of the region. There is however, a radius of refinement that is larger than the solution radius; this can be attributed to a constraint, imposed by the finite element software used \cite{BangerthHartmannKanschat2007,DealIIReference}, to have only a single hanging node per edge. This constraint actually allows for an advantageous feature that the solution can move in this refined region without requiring the mesh to be refined further; in Figure \ref{fig:movingGauss2} the solution has moved slightly, but the mesh is still exactly the same.

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/movingGaussian01.png}
	\caption{t = 0s}
	\label{fig:movingGauss1}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/movingGaussian02.png}
	\caption{t = 0.067s}
	\label{fig:movingGauss2}
	\end{subfigure}

	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/movingGaussian03.png}
	\caption{t = 0.5s}
	\label{fig:movingGauss3}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/movingGaussian04.png}
	\caption{t = 1.33s}
	\label{fig:movingGauss4}
	\end{subfigure}
\caption{Traveling Gaussian solution with dynamic AMR}
\label{fig:revolvingGaussian}
\end{figure}

As time progresses, the mesh lags behind the solution. The lagging is caused by the fact that the error vector used to determine which cells need refinement is an average vector of each time step's error vector since the last refinement (Equation \ref{eq:averageError}); naturally, where the solution had been in the past would also have a large error. 

As opposed to Figure \ref{fig:wakeSolution}, this figure shows no wake in the solution path. This is attributed to the spatial size limit, which was not present in Figure \ref{fig:wakeSolution}. In addition, the mesh that is behind the solution is coarsened as the solution moves away from that region so that under utilized Degrees of Freedom (solution unknowns) can be freed.

Figure \ref{fig:maxwellian} shows four snapshots in the evolution of the second solution referred to as the ``Reactor" solution. This solution differs from the Traveling Gaussian solution in that the initial condition is smooth and thus the initial mesh is not able to anticipate where refinement will be needed.

Figure \ref{fig:maxwellian1} shows the initial condition for this solution, and the refinement seems to be sporadically placed close to the center of the domain. As the Gaussian peak begins to emerge in Figure \ref{fig:maxwellian2}, the refinement drifts from the center of the domain to the left half of the domain. By the time Figure \ref{fig:maxwellian3} is reached, the peak is visible to the eye and the mesh is concentrated around the peak much like in Figure \ref{fig:revolvingGaussian}. After this point the mesh is more or less static except for a few small perturbations. Figure \ref{fig:maxwellian4} is near the end of the time interval for this solution; the peak is clearly visible and will start to decrease in magnitude from this time forward. The mesh is not expected to change back to the initial state because an exponential is never zero and thus the peak will always be present.

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/maxwellian01.png}
	\caption{t = 0s}
	\label{fig:maxwellian1}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/maxwellian02.png}
	\caption{t = 0.047s}
	\label{fig:maxwellian2}
	\end{subfigure}

	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/maxwellian03.png}
	\caption{t = 0.094s}
	\label{fig:maxwellian3}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/maxwellian04.png}
	\caption{t = 0.914s}
	\label{fig:maxwellian4}
	\end{subfigure}
\caption{Reactor solution with dynamic AMR}
\label{fig:maxwellian}
\end{figure}

This section has been a qualitative analysis of how the propagating mesh will handle certain types of problems. The next section provides a quantitative analysis of how much better the propagating mesh can be when compared to conventional techniques of uniformly refined meshes and refinement at every step.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           Section V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Performance of Propagating Mesh}

In this section, the same manufactured solutions analyzed in the previous section are computed using a third-order time integrator (SDIRK33) with 128 time steps in the interval $t\in[0,1s]$. Three angle tolerances (45$^\circ$, 10$^\circ$, 1$^\circ$) are compared against a static uniformly refined mesh with the metrics of ``Number of DOFs" (memory consumption) and ``CPU Time" (computation time).

Figure \ref{fig:travelingGauss} shows the solution error versus problem size (Number DOFs) for both solution variables. For each variable, the adapted mesh always produces a smaller system to solve, and gives an accurate result. The result is not surprising since the spatial mesh for the Traveling Gaussian solution is mostly coarse except for a concentrated area of the domain. There seems to be close agreement between the $1^\circ$ \& $10^\circ$ tolerances. However, the $45^\circ$ tolerance seems to begin to plateau; this can be an indicator that $45^\circ$ is not a well chosen tolerance. Also, note that in Figure \ref{fig:travelingGauss1} the convergence rate starts to plateau similar to what was seen in the discussion from the previous section. This is an indication that the temporal discretization's contribution to the solution error is beginning to dominate and that for this level of refinement, the time step size is too large.

\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/travelingGauss1.png}
  \caption{$\phi$}
  \label{fig:travelingGauss1}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/travelingGauss2.png}
  \caption{T}
  \label{fig:travelingGauss2}
  \end{subfigure}
\caption{Convergence of Traveling Gaussian solution vs. system size}
\label{fig:travelingGauss}
\end{figure}

Figure \ref{fig:travelingGauss_time} consists of the same set up, but the solution error is compared against the run time. Incidentally the added time to construct the system of equations on the adaptively refined mesh is not more than the time required to solve the larger system of the uniformly refined mesh. Again this result is not surprising since the size of the uniformly refined system is roughly $10\times$ as large as the adaptively refined systems. Even with the added complexities of hanging nodes and coupling terms, the adaptively refined meshes seem to win.

\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/travelingGauss1_time.png}
  \caption{$\phi$}
  \label{fig:travelingGauss1_time}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/travelingGauss2_time.png}
  \caption{T}
  \label{fig:travelingGauss2_time}
  \end{subfigure}
\caption{Error of Traveling Gaussian solution vs. run time}
\label{fig:travelingGauss_time}
\end{figure}

Switching to the Reactor solution in Figure \ref{fig:propagatingMeshMaxwellian}, the same type of behavior is observed. What is now more pronounced is that the 45$^\circ$ tolerance is not fine enough to resolve the changing solution. Especially for the second variable in Figure \ref{fig:propagatingMeshMaxwellian2}, the 45$^\circ$ tolerance plateaus very early. By contrast, the $1^\circ \& 10^\circ$ tolerances seem to coincide well with each other.

\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/propagatingMeshMaxwellian1.png}
  \caption{$\phi$}
  \label{fig:propagatingMeshMaxwellian1}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/propagatingMeshMaxwellian2.png}
  \caption{T}
  \label{fig:propagatingMeshMaxwellian2}
  \end{subfigure}
\caption{Convergence of Reactor solution vs. system size}
\label{fig:propagatingMeshMaxwellian}
\end{figure}

Figure \ref{fig:propagatingMeshMaxwellian_time} shows the same problem set, but compared against the run time. The results are not as clear as in the previous cases; it seems that after a certain point, the refined mesh with a 1$^\circ$ tolerance takes just as long to compute as the uniformly refined static mesh. Since the size of the adapted system is close to $10\times$ smaller than the uniformly refined system, the increase in time must come from the assembly process and not the linear solver. This result is not very reassuring to previous algorithms since the 1$^\circ$ tolerance would be faster than the methods presented in the literature. From the computational time results, the propagating mesh algorithm seems to produce a solution faster than previous algorithms would.


\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/propagatingMeshMaxwellian1_time.png}
  \caption{$\phi$}
  \label{fig:propagatingMeshMaxwellian1_time}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/propagatingMeshMaxwellian2_time.png}
  \caption{T}
  \label{fig:propagatingMeshMaxwellian2_time}
  \end{subfigure}
\caption{Error of Reactor solution vs. run time}
\label{fig:propagatingMeshMaxwellian_time}
\end{figure}

Overall, the algorithm for propagating a spatial mesh across several time steps seems to utilize the computational resources more efficiently than previously proposed algorithms would. It is shown that an acceptable value for $\theta_\mathrm{tol}$ is close to 10$^\circ$. The resources freed from this algorithm can either be used to solve the problem more accurately or in a faster time. 

